Rails.application.routes.draw do
  devise_for :users
  get 'pages/index'
  get 'pages/attendance'
  get 'pages/ledger'
  get 'pages/marks'
  devise_scope :user do
    root to: 'devise/sessions#new'
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
end
