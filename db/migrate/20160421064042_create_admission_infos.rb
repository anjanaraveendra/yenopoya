class CreateAdmissionInfos < ActiveRecord::Migration
  def change
    create_table :admission_infos do |t|
      t.references :user, index: true
      t.string :category
      t.string :religion
      t.string :father_name
      t.string :branch
      t.string :phone
      t.string :address

      t.timestamps null: false
    end
    add_foreign_key :admission_infos, :users
  end
end
