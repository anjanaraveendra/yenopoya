class AddUserIdToLedgerInfo < ActiveRecord::Migration
  def change
    add_reference :ledger_infos, :user, index: true
  end
end
