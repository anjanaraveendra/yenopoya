class CreateLedgerInfos < ActiveRecord::Migration
  def change
    create_table :ledger_infos do |t|
      t.string :doc_no
      t.date :release_date
      t.string :narration
      t.string :status
      t.string :amount

      t.timestamps null: false
    end
  end
end
