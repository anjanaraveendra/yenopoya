class CreateMarksInfos < ActiveRecord::Migration
  def change
    create_table :marks_infos do |t|
      t.string :max_marks
      t.string :min_marks
      t.string :marks_obtained
      t.references :user, index: true
      t.string :branch
      t.string :semister
      t.string :exam_type
      t.string :subject

      t.timestamps null: false
    end
    add_foreign_key :marks_infos, :users
  end
end
