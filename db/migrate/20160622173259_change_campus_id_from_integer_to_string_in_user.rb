class ChangeCampusIdFromIntegerToStringInUser < ActiveRecord::Migration
  def change
  	change_column :users, :campus_id, :string
  end
end
