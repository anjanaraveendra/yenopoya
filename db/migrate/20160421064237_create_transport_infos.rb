class CreateTransportInfos < ActiveRecord::Migration
  def change
    create_table :transport_infos do |t|
      t.string :route
      t.string :pickup_location

      t.timestamps null: false
    end
  end
end
