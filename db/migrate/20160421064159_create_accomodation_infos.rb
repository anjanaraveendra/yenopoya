class CreateAccomodationInfos < ActiveRecord::Migration
  def change
    create_table :accomodation_infos do |t|
      t.references :user, index: true
      t.string :hostel_name
      t.integer :room_number
      t.string :floor_number
      t.string :food

      t.timestamps null: false
    end
    add_foreign_key :accomodation_infos, :users
  end
end
