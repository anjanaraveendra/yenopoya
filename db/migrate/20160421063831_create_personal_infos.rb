class CreatePersonalInfos < ActiveRecord::Migration
  def change
    create_table :personal_infos do |t|
      t.string :usn
      t.string :course
      t.string :semister
      t.string :nationality
      t.string :state
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :personal_infos, :users
  end
end
