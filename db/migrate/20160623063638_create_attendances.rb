class CreateAttendances < ActiveRecord::Migration
  def change
    create_table :attendances do |t|
      t.string :subject_name
      t.string :status
      t.string :from
      t.string :to
      t.datetime :day
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :attendances, :users
  end
end
