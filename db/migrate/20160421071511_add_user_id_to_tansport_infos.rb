class AddUserIdToTansportInfos < ActiveRecord::Migration
  def change
    add_reference :transport_infos, :user, index: true
  end
end
