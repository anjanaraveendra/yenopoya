class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :date_of_birth, :date
    add_column :users, :blood_group, :string
    add_column :users, :campus_id, :integer
  end
end
