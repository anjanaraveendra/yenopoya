# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160702040730) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accomodation_infos", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "hostel_name"
    t.integer  "room_number"
    t.string   "floor_number"
    t.string   "food"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "accomodation_infos", ["user_id"], name: "index_accomodation_infos_on_user_id", using: :btree

  create_table "admission_infos", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "category"
    t.string   "religion"
    t.string   "father_name"
    t.string   "branch"
    t.string   "phone"
    t.string   "address"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "admission_infos", ["user_id"], name: "index_admission_infos_on_user_id", using: :btree

  create_table "attendances", force: :cascade do |t|
    t.string   "subject_name"
    t.string   "status"
    t.string   "from"
    t.string   "to"
    t.datetime "day"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "sem"
    t.string   "at_no"
  end

  add_index "attendances", ["user_id"], name: "index_attendances_on_user_id", using: :btree

  create_table "ledger_infos", force: :cascade do |t|
    t.string   "doc_no"
    t.date     "release_date"
    t.string   "narration"
    t.string   "status"
    t.string   "amount"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "user_id"
  end

  add_index "ledger_infos", ["user_id"], name: "index_ledger_infos_on_user_id", using: :btree

  create_table "marks_infos", force: :cascade do |t|
    t.string   "max_marks"
    t.string   "min_marks"
    t.string   "marks_obtained"
    t.integer  "user_id"
    t.string   "branch"
    t.string   "semister"
    t.string   "exam_type"
    t.string   "subject"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "marks_infos", ["user_id"], name: "index_marks_infos_on_user_id", using: :btree

  create_table "personal_infos", force: :cascade do |t|
    t.string   "usn"
    t.string   "course"
    t.string   "semister"
    t.string   "nationality"
    t.string   "state"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "personal_infos", ["user_id"], name: "index_personal_infos_on_user_id", using: :btree

  create_table "transport_infos", force: :cascade do |t|
    t.string   "route"
    t.string   "pickup_location"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "user_id"
  end

  add_index "transport_infos", ["user_id"], name: "index_transport_infos_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name"
    t.date     "date_of_birth"
    t.string   "blood_group"
    t.string   "campus_id"
    t.string   "opening_balance"
    t.integer  "parent_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "accomodation_infos", "users"
  add_foreign_key "admission_infos", "users"
  add_foreign_key "attendances", "users"
  add_foreign_key "marks_infos", "users"
  add_foreign_key "personal_infos", "users"
end
