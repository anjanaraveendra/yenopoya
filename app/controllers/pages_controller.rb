class PagesController < ApplicationController
  require 'will_paginate/array'

  def index
  end

  def marks
  	if params[:exam_type]
  		if current_user.child.present? 
      	@marks = current_user.child.marks_infos.where('exam_type = ? and semister = ?', params["exam_type"], params["semister"])
    	else
    		@marks = current_user.marks_infos.where('exam_type = ? and semister = ?', params["exam_type"], params["semister"])
    	end
    end
  end

  def attendance
    if params[:from].present?
      from, to = Date.strptime(params[:from], '%m/%d/%Y'), Date.strptime(params[:to], '%m/%d/%Y')
      arr = (from..to).to_a
      if current_user.child.present? 
        @attendances = Attendance.where(user_id: current_user.child.id).where(day: arr).group_by { |a| a.day }
      else
        @attendances = Attendance.where(user_id: current_user.id).where(day: arr).group_by { |a| a.day }
      end
    else
      if current_user.child.present?
        @attendances = Attendance.where(user_id: current_user.child.id).group_by { |a| a.day }
      else
        @attendances = Attendance.where(user_id: current_user.id).group_by { |a| a.day }
      end
    end
    params[:sem] = 'I' if params[:sem].blank?
    @attendances_search = current_user.attendances.where(sem: params[:sem]).group_by { |a| a.subject_name }
    @total_classes = current_user.attendances.where(sem: params[:sem]).group_by { |a| a.subject_name }
    @classes_present = current_user.attendances.where(sem: params[:sem], status: "P").group_by { |a| a.subject_name }
    @classes_absent = current_user.attendances.where(sem: params[:sem], status: "A").group_by { |a| a.subject_name }
    @attendances_keys = @attendances.keys.sort.reverse.paginate(page: params[:page], per_page: 5)
  end
end
