# == Schema Information
#
# Table name: marks_infos
#
#  id             :integer          not null, primary key
#  max_marks      :string
#  min_marks      :string
#  marks_obtained :string
#  user_id        :integer
#  branch         :string
#  semister       :string
#  exam_type      :string
#  subject        :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class MarksInfo < ActiveRecord::Base
  belongs_to :user
end
