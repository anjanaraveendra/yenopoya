# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string
#  date_of_birth          :date
#  blood_group            :string
#  campus_id              :string
#  opening_balance        :string
#  parent_id              :integer
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable,
         authentication_keys: [:login]
  validates :campus_id, uniqueness: true
  attr_accessor :login

  def login=(login)
    @login = login
  end

  def login
    @login || self.campus_id || self.email
  end

  has_one :child, class_name: 'User',
                      foreign_key: 'parent_id'
  belongs_to :parent, class_name: 'User'

  has_one :personal_info, dependent: :destroy
  delegate :usn, :course, :semister, :nationality, :state, to: :personal_info, allow_nil: true

  has_one :admission_info, dependent: :destroy
  delegate :category, :religion, :father_name, :branch, :phone, :address, to: :admission_info, allow_nil: true

  has_one :accomodation_info, dependent: :destroy
  delegate :hostel_name, :room_number, :floor_number, :food, to: :accomodation_info, allow_nil: true

  has_one :transport_info, dependent: :destroy
  delegate :route, :pickup_location, to: :transport_info, allow_nil: true

  has_many :ledger_infos, dependent: :destroy
  has_many :marks_infos, dependent: :destroy
  has_many :attendances, dependent: :destroy

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      User.find_by(email: login) || User.find_by(campus_id: login)
    else
      if conditions[:campus_id].nil?
        where(conditions).first
      else
        where(campus_id: conditions[:campus_id]).first
      end
    end
  end
end
