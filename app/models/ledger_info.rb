# == Schema Information
#
# Table name: ledger_infos
#
#  id           :integer          not null, primary key
#  doc_no       :string
#  release_date :date
#  narration    :string
#  status       :string
#  amount       :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  user_id      :integer
#

class LedgerInfo < ActiveRecord::Base
  belongs_to :user
end
