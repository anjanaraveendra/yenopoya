# == Schema Information
#
# Table name: accomodation_infos
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  hostel_name  :string
#  room_number  :integer
#  floor_number :string
#  food         :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class AccomodationInfo < ActiveRecord::Base
  belongs_to :user
end
