# == Schema Information
#
# Table name: personal_infos
#
#  id          :integer          not null, primary key
#  usn         :string
#  course      :string
#  semister    :string
#  nationality :string
#  state       :string
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class PersonalInfo < ActiveRecord::Base
  belongs_to :user
end
