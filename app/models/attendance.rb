# == Schema Information
#
# Table name: attendances
#
#  id           :integer          not null, primary key
#  subject_name :string
#  status       :string
#  from         :string
#  to           :string
#  day          :datetime
#  user_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Attendance < ActiveRecord::Base
  belongs_to :user
end
