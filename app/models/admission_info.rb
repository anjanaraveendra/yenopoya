# == Schema Information
#
# Table name: admission_infos
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  category    :string
#  religion    :string
#  father_name :string
#  branch      :string
#  phone       :string
#  address     :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class AdmissionInfo < ActiveRecord::Base
  belongs_to :user
end
