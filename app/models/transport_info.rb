# == Schema Information
#
# Table name: transport_infos
#
#  id              :integer          not null, primary key
#  route           :string
#  pickup_location :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  user_id         :integer
#

class TransportInfo < ActiveRecord::Base
end
