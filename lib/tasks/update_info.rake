namespace :update_user_info do
  desc 'updating the student personal admission accomodation  transport info'
  task update_user: :environment do
    client = TinyTds::Client.new host: '23.91.70.62', username: 'appsys', password: 'juhEw815Omgqv258T', database: 'CLOUDSYS_YIT', port: 1433, timeout: 10_000
    user_data = client.execute('select * from StudentMaster')
    student_ids = []
    user_data.to_enum.with_index(1) do |user, index|
      email = user['s_contact_email'].blank? ? "#{user['s_usn']}_yenopoya@gmail.com".delete(' ') : user['s_contact_email']
      unless User.pluck(:email).include? email
        address = begin
          user['s_present_add1'] + ' ' + user['s_present_add2'] + ' ' + user['s_present_add3'] + ' ' + user['s_present_city'] + ' ' + user['s_present_state'] + ' ' + user['s_present_pin']
        rescue
          nil
        end
        campus_id = user['s_regno']
        blood_group = user['s_blood_group']
        name = user['s_name']
        date_of_birth = user['s_dob']
        opening_balance = user['s_opbal']
        usn = user['s_usn']
        course = user['s_course']
        semister = user['s_semester']
        nationality = user['s_nationality']
        state = user['s_present_state']
        category = user['s_obc_group']
        religion = user['s_religion']
        father_name = user['s_fathers_name']
        branch = user['s_course']
        phone = user['s_present_contactno']
        hostel_name = user['s_hostel']
        room_number = user['s_floor']
        floor_number = user['s_floor']
        food = user['s_messtype']
        route = user['s_route']
        pickup_location = user['s_pickuppoint']

        parent_email = "#{user['s_usn']}_parent@gmail.com".delete(' ')
        unless User.pluck(:email).include? parent_email
          parent = User.new(email: parent_email, password: 'parentpassword', campus_id: "#{campus_id}@parent")
          if parent.save
            student_ids << campus_id
            student = User.new(email: email, password: 'testpassword', campus_id: campus_id, blood_group: blood_group, name: name, date_of_birth: date_of_birth, opening_balance: opening_balance, parent_id: parent.id)
            if student.save
              PersonalInfo.create(usn: usn, course: course, semister: semister, nationality: nationality, state: state, user_id: student.id)
              AdmissionInfo.create(category: category, religion: religion, father_name: father_name, branch: branch, phone: phone, address: address, user_id: student.id)
              AccomodationInfo.create(hostel_name: hostel_name, room_number: room_number, floor_number: floor_number, food: food, user_id: student.id)
              TransportInfo.create(route: route, pickup_location: pickup_location, user_id: student.id)
            else
              p student.errors.full_messages
            end
          else
              p parent.errors.full_messages
          end 
          puts "User #{index} created"
        end
      end
    end
    student_ids = student_ids.join(',')
    client.execute('update syb from StudentMaster where s_regno (#{student_ids})')
    puts '************** User Info Updated ***************'
  end
end
