namespace :update_marks_info do
  desc 'updating student marks info'
  task update_marks: :environment do
    client = TinyTds::Client.new host: '23.91.70.62', username: 'appsys', password: 'juhEw815Omgqv258T', database: 'CLOUDSYS_YIT', port: 1433, timeout: 10000
    User.all.each.with_index(1) do |user, index|
      marks_infos = client.execute("select * from MARKS where m_regno='#{user.campus_id}'")
      marks_infos.each do |marks_info|
        max_marks = marks_info['m_max_mark']
        min_marks = marks_info['m_min_mark']
        marks_obtained = marks_info['m_marks_obtained']
        semister = marks_info['m_sem']
        branch = marks_info['m_course']
        exam_type = marks_info['m_exam']
        subject = marks_info['m_subject']
        user.marks_infos.create(max_marks: max_marks, min_marks: min_marks, marks_obtained: marks_obtained, semister: semister, branch: branch, exam_type: exam_type, subject: subject)
      end
      puts "User #{index} marks info updated"
    end
    puts '************** Completed *********************'
  end
end
