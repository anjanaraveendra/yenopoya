namespace :update_ledger_info do
  desc 'updating studen ledger info'
  task update_ledger: :environment do
    client = TinyTds::Client.new host: '23.91.70.62', username: 'appsys', password: 'juhEw815Omgqv258T', database: 'CLOUDSYS_YIT', port: 1433, timeout: 10000
    User.all.each.with_index(1) do |user, index|
      ledger_infos = client.execute("select * from fatran where ft_ac_code='#{user.campus_id}'")
      ledger_infos.each do |ledger_info|
        doc_no = ledger_info['ft_docno']
        release_date = ledger_info['ft_realisedate']
        narration = ledger_info['ft_narration']
        status = ledger_info['ft_dbcr']
        amount = ledger_info['ft_amount']
        user.ledger_infos.create(doc_no: doc_no, release_date: release_date, narration: narration, status: status, amount: amount)
      end
      puts "User #{index} ledger info updated"
    end
    puts '************** Completed *********************'
  end
end
