namespace :update_attendance_info do
  desc 'updating student attendance info'
  task update_attendance: :environment do
    at_nos = []
    client = TinyTds::Client.new host: '23.91.70.62', username: 'appsys', password: 'juhEw815Omgqv258T', database: 'CLOUDSYS_YIT', port: 1433, timeout: 10000
    User.all.each.with_index(1) do |user, index|
      attendance_infos = client.execute("select * from AttendanceNew where at_regno='#{user.campus_id}' and at_sync = 'true' ")
      attendance_infos.each do |attendance_info|
        subject_name = attendance_info['at_subject_name']
        status = attendance_info['at_status']
        from = attendance_info['at_from_time']
        to = attendance_info['at_to_time']
        sem = attendance_info['at_sem']
        day = attendance_info['at_date'].to_date if attendance_info['at_date'].present?
        at_no = attendance_info['at_no']
        at_deleted = attendance_info['at_deleted']
        attendance = user.attendances.where(at_no: at_no).first
        if attendance.present? && at_deleted
          attendance.destroy
        else 
          user.attendances.create(subject_name: subject_name, status: status, from: from, to: to, day: day, sem: sem, at_no: at_no)
        end
      end
      puts "User #{index} attendance info updated"
    end
    at_nos = Attendance.all.map(&:at_no).join(',')
    client.execute("update AttendanceNew set at_sync = 'false' where at_no in (#{at_nos})")
    client.close
    puts '************** Completed *********************'
  end
end